/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 23.01.2018
 *
 * GoCommand
 */

import java.util.*;
//package might_and_magic;

class GoCommand implements IExecute
{
	private String zweitesWort=null;
	
	private Game forbiddenGame=null;
	
	// -----------------
	
	public GoCommand(Game forbiddenGame)
	{
		this.forbiddenGame=forbiddenGame;
		//this.zweitesWort=new String();
	}
	
	public GoCommand()
	{
		//this.zweitesWort=new String();
	}
	
	//forbiddenGame=new Game();
	
	
	// get / set ---------------
	
	public String getZweitesWort()
	{
		if(null!=zweitesWort)
		{
			return zweitesWort;
		}
		return "?";
	}
	
	//overrides
	
	//Kein toString override: gitb zu Testzwecken das Kommando namentlich zur�ck.
	@Override
	public String bezeichner()
	{
		return "gehe durch ";
	}
	
	@Override
	public String execute()
	{
		/*
		Das Go-Kommando soll den Ziel Raum, wo
		
		
		*/
		return "f�hre aus:";
	}
	
	@Override
	public void setZweitesWort(String zweitesWort)
	{
		
		this.zweitesWort=zweitesWort;				//zweitesWort.split();
	}
	
	@Override
	public int hashCode()
	{
		return "gehe durch".hashCode();
	}

}
