/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 23.01.2018
 *
 * DescriptionCommand
 */

import java.util.*;
//package might_and_magic;

class DescriptionCommand implements IExecute
{
	private String zweitesWort=null;
	
	private Game forbiddenGame=null;
	
	// -----------------
	
	public DescriptionCommand(Game forbiddenGame)
	{
		this.forbiddenGame=forbiddenGame;
		//this.zweitesWort=new String();
	}
	
	public DescriptionCommand()
	{
		//this.zweitesWort=new String();
	}
	
	
	//forbiddenGame=new Game();
	
	
	// get / set ---------------
	
	public String getZweitesWort()
	{
		if(null!=zweitesWort)
		{
			return zweitesWort;
		}
		return "?";
	}
	
	//overrides
	
	//Kein toString override: gibt zu Testzwecken das Kommando namentlich zur�ck.
	@Override
	public String bezeichner()
	{
		return "beschreibe ";
	}

	
	@Override
	public String execute()
	{
		return "f�hre aus:";
	}
	
	@Override
	public void setZweitesWort(String zweitesWort)
	{
		
		this.zweitesWort=zweitesWort;				//zweitesWort.split();
	}
	
	@Override
	public int hashCode()
	{
		return "beschreibe".hashCode();
	}


}
