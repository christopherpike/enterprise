/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 23.01.2018
 *
 * UseCommand
 */

import java.util.*;
//package might_and_magic;

class UseCommand implements IExecute
{
	private String zweitesWort=null;
	
	private Game forbiddenGame=null;
	
	// -----------------
	
	
	//zweitesWort=new String();
	
	//forbiddenGame=new Game();
	
	
	// get / set ---------------
	
	public String getZweitesWort()
	{
		if(null!=zweitesWort)
		{
			return zweitesWort;
		}
		return "?";
	}
	
	//overrides
	
	//Kein toString override: gitb zu Testzwecken das Kommando namentlich zur�ck.
	@Override
	public String bezeichner()
	{
		return "benutze ";
	}
	
	@Override
	public String execute()
	{
		return "f�hre aus:";
	}
	
	@Override
	public void setZweitesWort(String zweitesWort)
	{
		
		this.zweitesWort=zweitesWort;				//zweitesWort.split();
	}
	
	@Override
	public int hashCode()
	{
		return "benutze".hashCode();
	}


}
