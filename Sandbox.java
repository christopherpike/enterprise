/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 22.01.2018
 *
 * Sandbox
 */

import java.io.*;
import java.util.*;

class Sandbox
{
	public static void main(String[]arguments)
	{

	//Erzeugt HashMap als Lager f�r alle Gegenst�nde im Spiel, um davon Gegenst�nde in die itemList eines Raum zu putten. M�ssen dann hier removed werden.
	HashMap<String,Gegenstand>alleGegenstaende=new HashMap<String,Gegenstand>();

	System.out.println(alleGegenstaende.put("Ausgangs-Schl�ssel",new Gegenstand("Ausgangs-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
						2,false,null,null,null)) );

	System.out.println("Gr��e der HashMap : "+alleGegenstaende.size());
	
	Set<String>durchlauf=alleGegenstaende.keySet();
	
	System.out.println("Achtung:");
	for(String str:durchlauf)
	{
		System.out.println(str);
	}

	HashMap<String,Raum>alleRaeume=new HashMap<String,Raum>();

	alleRaeume.put	("Werkzeugkammer",
						new Raum("Werkzeugkammer",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);
	
	//Raum 0 auf meinem Plan
		alleRaeume.put	("Werkzeugkammer",
						new Raum("Werkzeugkammer",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 1 auf meinem Plan
		alleRaeume.put	("K�nigskammer",
						new Raum("K�nigskammer",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 2 auf meinem Plan
		alleRaeume.put	("Prinzessinskammer",
						new Raum("Prinzessinskammer",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 3 auf meinem Plan
		alleRaeume.put	("Abort",
						new Raum("Abort",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 4 auf meinem Plan
		alleRaeume.put	("Verlie�",
						new Raum("Verlie�",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 5 auf meinem Plan
		alleRaeume.put	("Ratshalle",
						new Raum("Ratshalle",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 6 auf meinem Plan
		alleRaeume.put	("Vorratslager",
						new Raum("Vorratslager",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 7 auf meinem Plan
		alleRaeume.put	("K�che",
						new Raum("K�che",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 8 auf meinem Plan
		alleRaeume.put	("Speisesaal",
						new Raum("Speisesaal",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 9 auf meinem Plan
		alleRaeume.put	("Gefechtsraum",
						new Raum("Gefechtsraum",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 10 auf meinem Plan, das Ziel !!!!!!!!!!
		alleRaeume.put	("Freie",
						new Raum("Freie",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

	System.out.println("Das sind alle R�ume: "+alleRaeume.size());
	
	
	
		
		//Item 0 von meiner Liste
		alleGegenstaende.put("Ausgangs-Schl�ssel",new Gegenstand("Ausgangs-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,false,null,null,null));

		//Item 1 von meiner Liste
		alleGegenstaende.put("Gold-Schl�ssel",new Gegenstand("Gold-Schl�ssel","Ein Schl�ssel f�r einen Schatz.","Dieser Schl�ssel k�nnte in eine Holztruhe passen, wenn man so schaut.",
		2,false,null,null,null));
		
		//Item 2 von meiner Liste
		alleGegenstaende.put("Rot-Schl�ssel",new Gegenstand("Rot-Schl�ssel","Dietrich zum Herzen eines Fr�uleins.","Damit kann man in das Schlafgemach einer zuk�nftigen K�nigin gelangen.",
		2,false,null,null,null));
		
		//Item 3 von meiner Liste
		alleGegenstaende.put("Blau-Schl�ssel",new Gegenstand("Blau-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,false,null,null,null));
		
		//Item 4 von meiner Liste
		alleGegenstaende.put("Schwarz-Schl�ssel",new Gegenstand("Schwarz-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,false,null,null,null));
		
		//Item 5 von meiner Liste
		alleGegenstaende.put("Holztruhe",new Gegenstand("Holztruhe","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		0,false,null,null,null));
		
		//Item 6 von meiner Liste
		alleGegenstaende.put("K�stchen",new Gegenstand("K�stchen","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		0,false,null,null,null));
		
		//Item 7 von meiner Liste
		alleGegenstaende.put("Dose",new Gegenstand("Dose","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		0,false,null,null,null));
		
		//Item 8 von meiner Liste
		alleGegenstaende.put("Schaltulle",new Gegenstand("Schatulle","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		0,false,null,null,null));
		
		//Item 9 von meiner Liste
		alleGegenstaende.put("Apfel",new Gegenstand("Apfel","Reifer roter Apfel.","Super Nahrungsmittel. One apple a day keeps the doctor away :-)",
		3,true,null,null,null));
		
		//Item 10 von meiner Liste
		alleGegenstaende.put("Brotlaib",new Gegenstand("Brotlaib","Bernd das Brot.","Gesch�tz 967 Gramm, handgemacht, allerdings nicht mehr ganz frisch.",
		4,true,null,null,null));
		
		//Item 11 von meiner Liste
		alleGegenstaende.put("K�sest�ck",new Gegenstand("K�sest�ck","Irischer Cheedar.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		3,true,null,null,null));
		
		//Item 12 von meiner Liste
		alleGegenstaende.put("Schinkenkeule",new Gegenstand("Schinkenkeule","Geil Schinken.","Miss Piggy, wie sie jeder haben will. Mager und schweigsam. Aber mal ehlich, ich bin das Schwein. Und Vegitarier.",
		6,true,null,null,null));
		
		//Item 13 von meiner Liste
		alleGegenstaende.put("Wein",new Gegenstand("Wein","Rotwein im Kelch, Jahrgang 1736.","Alkohol macht alle hohl. Und konserviert von innen.",
		2,true,null,null,null));
		
		//Item 14 von meiner Liste
		alleGegenstaende.put("Federkiel",new Gegenstand("Federkiel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,false,null,null,null));
	
	Set<String>probe=alleGegenstaende.keySet();
	
	System.out.println("Achtung:");
	for(String str:durchlauf)
	{
		System.out.println(str);
	}
	
	System.out.println("Alle Gegenst�nde "+probe.size());
	
	System.out.println("Kommando Test");
	
	Parser parser=new Parser();

	parser.setBefehl("gehe durch",new GoCommand());

	parser.setBefehl("nimm",new TakeCommand());

	parser.setBefehl("beschreibe",new DescriptionCommand());

	parser.setBefehl("benutze",new UseCommand());
	
	//-----
	
	System.out.println("Sind die Kommandos auch gespeichert?");
	
	IExecute com=parser.getCommand("gehe durch");
	
	System.out.println(com.bezeichner());
	
	System.out.println(parser.getAlleBefehleAlsString());
		
	}
		
		


	/*
	R�ume
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	*/
}
