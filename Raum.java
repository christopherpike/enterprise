/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 22.01.2018
 *
 * Raum
 */

import java.util.*;

class Raum
{
	//Der Raumbezeichner, K�nigskammer
	private String name;
	
	//H�lt alle verf�gbaren Ausg�nge des Raums
	private HashMap<String,Raum>ausgaenge=null;
	
	//H�lt alle verf�gbaren Items des Raums
	private HashMap<Gegenstand,String>itemList=null;
	
	//Kurze Beschreibung des Raums, "Die Kammer des K�nigs."
	private String kurzBeschreibung;

	//Ausf�hrliche Beschreibung des Raums, "Des K�nigs privater Raum, in dem sein Bett und eine Anrichte steht, auf der ein h�lzernes K�stchen liegt."
	private String langBeschreibung;
	
	// -----------------------
	
	//Voller Konstruktor
	public Raum (
				String name,
				HashMap<String,Raum>ausgaenge,
				HashMap<Gegenstand,String>itemList,
				String kurzBeschreibung,
				String langBeschreibung
				)
	{
		this.name=name;
		this.ausgaenge=ausgaenge;
		this.itemList=itemList;
		this.kurzBeschreibung=kurzBeschreibung;
		this.langBeschreibung=langBeschreibung;
	}

	public Raum(String name)
	{
		this.name=name;
		ausgaenge=new HashMap<String,Raum>();
		itemList=new HashMap<Gegenstand,String>();
	}
	
	// get / set ---------------------
	
	//Liefert den Raumbezeichner
	public String getName()
	{
		return name;
	}
	
	//Liefert die Kurzbeschreibung f�r den Raum
	public String getKurzBeschreibung()
	{
		return this.kurzBeschreibung;
	}

	//Liefert die ausf�hliche Beschrebung f�r den Raum
	public String getLangBeschreibung()
	{
		return this.langBeschreibung;
	}
	
	// adder
	
	//F�gt einen Ausgang der auf einen Raum oder null zeigt, zu diesem Raum hinzu
	public Raum addAusgaenge(String ausgang,Raum zumRaum)
	{
		return ausgaenge.put(ausgang,zumRaum);
	}
	
	//F�gt einen Gegenstand zu diesem Raum hinzu
	public String addItemList(Gegenstand gegenstand,String gegenstandName)
	{
		return itemList.put(gegenstand,gegenstandName);
	}
	
	//overrides
	
	@Override
	public int hashCode()
	{
		//Weil ich nur den Namen des Raums als UNIQUE haben will.
		return name.hashCode();
	}
	
	@Override
	public String toString()
	{
		return "Dieser Raum hei�t "+getName()+", und die ausf�hrliche Beschreibung lautet: "+getLangBeschreibung()+" Es gibt "+

		ausgaenge.size()+" Ausg�nge, und "+itemList.size()+" Items.";
	}
	
	//equals
}
