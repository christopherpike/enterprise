/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 22.01.2018
 *
 * Gegenstand
 */

import java.io.*;
import java.util.*;

class Gegenstand
{
	//Gegenstandsname, soll UNIQUE sein
	private String name;
	
	//Kurze Beschreibung des Gegenstands
	private String kurzBeschreibung;
	
	//Ausf�hrliche Beschreibung des Gegenstands
	private String langBeschreibung;
	
	//Gewicht des Gegenstands, wenn 0 dann ist er unbeweglich, wenn 1 essbar, wenn mehr ....
	private int gewicht;
	
	//Essbar, k�nnte man vielleich einsparen
	private boolean essbar;
	
	//Erh�t die Lebenskraft, wenn gegessen
	private int energiewert;
	
	//Frage f�r das Item
	private String frageFuerItem;
	
	//Antwort f�r das Item
	private String antwortFuerItem;
	
	//Item das wenn die Antwort auf dei Itemfrage richtig ist, zur�ckgegeben wird = Schl�ssel f�r Ausgang
	private Gegenstand item;
	
	//------------------------

	//voller Konstruktor
	public Gegenstand(	String name,
						String kurzBeschreibung,
						String langBeschreibung,
						int gewicht,
						boolean essbar,
						String frageFuerItem,
						String antwortFuerItem,
						Gegenstand item
					)
	{
		this.name=name;
		this.kurzBeschreibung=kurzBeschreibung;
		this.langBeschreibung=langBeschreibung;
		this.gewicht=gewicht;
		this.essbar=essbar;
		this.frageFuerItem=frageFuerItem;
		this.antwortFuerItem=antwortFuerItem;
		this.item=item;
	}
	
	public Gegenstand(String name,int gewicht)
	{
		this.name=name;
		this.gewicht=gewicht;
	}
	
	
	// get / set
	
	//Liefert die Kurzbeschreibung f�r den Gegenstand
	public String getKurzBeschreibung()
	{
		return this.kurzBeschreibung;
	}
	
	//Liefert die ausf�hliche Beschrebung f�r den Gegenstand
	public String getLangBeschreibung()
	{
		return this.langBeschreibung;
	}
		
	//Liefert das Gewicht des Gegenstandes
	public int getGewicht()
	{
		return this.gewicht;
	}
	
	//String frageFuerItem;
	public boolean getEssbar()
	{
		return this.essbar;
	}
	
	//
	public void setEssbar(boolean essbar)
	{
		this.essbar=essbar;
	}
	
	// !!! Gef�hrlich
	public String getFrageFuerItem()
	{
		return this.frageFuerItem;
	}
	
	// !!! Gef�hrlich
	public void setFrageFuerItem(String frageFuerItem)
	{
		this.frageFuerItem=frageFuerItem;
	}
	
	// !!! Gef�hrlich
	public String getAntwortFuerItem()
	{
		return this.antwortFuerItem;
	}
	
	// !!! Gef�hrlich
	public void setAntwortFuerItem(String antwortFuerItem)
	{
		this.antwortFuerItem=antwortFuerItem;
	}
	
	//overrides
	
	@Override
	public int hashCode()
	{
		//this.name=name;
		//this.kurzBeschreibung=kurzBeschreibung;
		//this.langBeschreibung=langBeschreibung;
		//this.gewicht=gewicht;
		//this.essbar=essbar;
		//this.frageFuerItem=frageFuerItem;
		//this.antwortFuerItem=antwortFuerItem;
		//this.item=item;
		
		//Weil der Name Unique ist: sein soll
		return name.hashCode();
	}
	
	@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Gegenstand other = (Gegenstand) obj;
			if (antwortFuerItem == null) {
				if (other.antwortFuerItem != null)
					return false;
			} else if (!antwortFuerItem.equals(other.antwortFuerItem))
				return false;
			if (energiewert != other.energiewert)
				return false;
			if (essbar != other.essbar)
				return false;
			if (frageFuerItem == null) {
				if (other.frageFuerItem != null)
					return false;
			} else if (!frageFuerItem.equals(other.frageFuerItem))
				return false;
			if (gewicht != other.gewicht)
				return false;
			if (kurzBeschreibung == null) {
				if (other.kurzBeschreibung != null)
					return false;
			} else if (!kurzBeschreibung.equals(other.kurzBeschreibung))
				return false;
			if (langBeschreibung == null) {
				if (other.langBeschreibung != null)
					return false;
			} else if (!langBeschreibung.equals(other.langBeschreibung))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}


	//compareTo
	//@Override
	
	
	
	@Override
	public String toString()
	{
		return "Gegenstand x";
	}
}
