/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 22.01.2018
 *
 * Game
 */

import java.io.*;
import java.util.*;
//package might_and_magic;

class Game
{
	//H�lt den raum, in dem sich unser held aufh�lt
	private Raum aktuellerRaum;
	
	//H�lt den Held
	private Held held;
	
	//H�lt den Kommand-�bersetzer
	private Parser parser=null;
	
	//Der Spielname
	private final String name;
	
	//-----------------------
	
	public Game(Held held,String name)
	{
		this.aktuellerRaum=new Raum("H�hleneingang");		//Starpunkt ist der "H�hleneingang"
		this.held=new Held("Ben");//held;                	//Wird von aussen �bergeben
		this.parser=new Parser();                 			//Soll hier initialisiert werden
		this.name=name;                             		//Spielname
		
	}
	
	//get / set
	
	public Raum getAktuellerRaum()
	{
		return this.aktuellerRaum;
	}
	
	public boolean setAktuellerRaum(Raum aktuellerRaum)
	{
		this.aktuellerRaum=aktuellerRaum;
		return true;
	}
	
	public Held getHeld()
	{
		return held;
	}

	//keine set Held, aus Sicherheitsgr�nden

	//keine get/set Parser, aus Sicherheitsgr�nden
	
	public String getName()
	{
		return name;
	}
	
	//
	
	//Methoden

	//
	private void createWorld()
	{
		//--------------- Initialisierung Gegenst�nde

		//Erzeugt HashMap als Lager f�r alle Gegenst�nde im Spiel, um davon Gegenst�nde in die itemList eines Raum zu putten. M�ssen dann hier removed werden.
		HashMap<String,Gegenstand>alleGegenstaende=new HashMap<String,Gegenstand>();
		
		//Item 0 von meiner Liste
		alleGegenstaende.put("Ausgangs-Schl�ssel",new Gegenstand("Ausgangs-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,false,null,null,null));

		//Item 1 von meiner Liste
		alleGegenstaende.put("Gold-Schl�ssel",new Gegenstand("Gold-Schl�ssel","Ein Schl�ssel f�r einen Schatz.","Dieser Schl�ssel k�nnte in eine Holztruhe passen, wenn man so schaut.",
		2,false,null,null,null));
		
		//Item 2 von meiner Liste
		alleGegenstaende.put("Rot-Schl�ssel",new Gegenstand("Rot-Schl�ssel","Dietrich zum Herzen eines Fr�uleins.","Damit kann man in das Schlafgemach einer zuk�nftigen K�nigin gelangen.",
		2,false,null,null,null));
		
		//Item 3 von meiner Liste
		alleGegenstaende.put("Blau-Schl�ssel",new Gegenstand("Blau-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,false,null,null,null));
		
		//Item 4 von meiner Liste
		alleGegenstaende.put("Schwarz-Schl�ssel",new Gegenstand("Schwarz-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,false,null,null,null));
		
		//Item 5 von meiner Liste
		alleGegenstaende.put("Holztruhe",new Gegenstand("Holztruhe","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		0,false,null,null,null));
		
		//Item 6 von meiner Liste
		alleGegenstaende.put("K�stchen",new Gegenstand("K�stchen","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		0,false,null,null,null));
		
		//Item 7 von meiner Liste
		alleGegenstaende.put("Dose",new Gegenstand("Dose","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		0,false,null,null,null));
		
		//Item 8 von meiner Liste
		alleGegenstaende.put("Schaltulle",new Gegenstand("Schatulle","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		0,false,null,null,null));
		
		//Item 9 von meiner Liste
		alleGegenstaende.put("Apfel",new Gegenstand("Apfel","Reifer roter Apfel.","Super Nahrungsmittel. One apple a day keeps the doctor away :-)",
		3,true,null,null,null));
		
		//Item 10 von meiner Liste
		alleGegenstaende.put("Brotlaib",new Gegenstand("Brotlaib","Bernd das Brot.","Gesch�tz 967 Gramm, handgemacht, allerdings nicht mehr ganz frisch.",
		4,true,null,null,null));
		
		//Item 11 von meiner Liste
		alleGegenstaende.put("K�sest�ck",new Gegenstand("K�sest�ck","Irischer Cheedar.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		3,true,null,null,null));
		
		//Item 12 von meiner Liste
		alleGegenstaende.put("Schinkenkeule",new Gegenstand("Schinkenkeule","Geil Schinken.","Miss Piggy, wie sie jeder haben will. Mager und schweigsam. Aber mal ehlich, ich bin das Schwein.",
		6,true,null,null,null));
		
		//Item 13 von meiner Liste
		alleGegenstaende.put("Wein",new Gegenstand("Wein","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,true,null,null,null));
		
		//Item 14 von meiner Liste
		alleGegenstaende.put("Federkiel",new Gegenstand("Federkiel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,false,null,null,null));
		
		
		//Raum ItemList
		
		
		
		
		
		
		Gegenstand ausgangsschl=new Gegenstand("Ausgangs-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		2,false,null,null,null);

		new Gegenstand("Ausgangs-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",2,false,null,null,null);
		
		//new Gegenstand("Ausgangs-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",2,false,null,null,null);
		
		//new Gegenstand("Ausgangs-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",2,false,null,null,null);
		
		//new Gegenstand("Ausgangs-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",2,false,null,null,null);
		
		

		//alleGegenstaende.put(new Gegenstand("Ausgangs-Schl�ssel","Der Schl�ssel f�r den Ausgang.","Mit diesem Schl�ssel kannst du das Geb�ude verlassen",
		//										2,0,null,null,null))

		/*


		*/

		HashMap<String,Raum>alleRaeume=new HashMap<String,Raum>();

		//Raum 0 auf meinem Plan
		alleRaeume.put	("Werkzeugkammer",
						new Raum("Werkzeugkammer",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 1 auf meinem Plan
		alleRaeume.put	("K�nigskammer",
						new Raum("K�nigskammer",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 2 auf meinem Plan
		alleRaeume.put	("Prinzessinskammer",
						new Raum("Prinzessinskammer",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);
		
		//Raum 3 auf meinem Plan
		alleRaeume.put	("Abort",
						new Raum("Abort",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);
		
		//Raum 4 auf meinem Plan
		alleRaeume.put	("Verlie�",
						new Raum("Verlie�",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 5 auf meinem Plan
		alleRaeume.put	("Ratshalle",
						new Raum("Ratshalle",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 6 auf meinem Plan
		alleRaeume.put	("Vorratslager",
						new Raum("Vorratslager",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);

		//Raum 7 auf meinem Plan
		alleRaeume.put	("K�che",
						new Raum("K�che",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);
						
		//Raum 8 auf meinem Plan
		alleRaeume.put	("Speisesaal",
						new Raum("Speisesaal",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);
						
		//Raum 9 auf meinem Plan
		alleRaeume.put	("Gefechtsraum",
						new Raum("Gefechtsraum",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);
						
		//Raum 10 auf meinem Plan, das Ziel !!!!!!!!!!
		alleRaeume.put	("Freie",
						new Raum("Freie",null,null,"Die Werkzeugkammer des Schloss.","In der Kammer kannst du bei dieser Beleuchtung nur die T�r erkennen.")
						);
		
		// ------------- Parser f�llen
		
  		parser.setBefehl("gehe durch",new GoCommand());
		
		parser.setBefehl("nimm",new TakeCommand());
		
		parser.setBefehl("beschreibe",new DescriptionCommand());
		
		parser.setBefehl("benutze",new UseCommand());
		
		// -----------
	}
	
	//
	private void gameMenu()
	{
		System.out.println("Spielmenue:");
	}
	
	//
	public static void goRaum(String raumname_oder_ausgangsname)
	{
		System.out.println("Du gingst durch T�r ausgangsname.");
	}
	
	//
	private void init()
	{
		System.out.println("Init!");
	}
	
	//
	public static void raumBeschreibung()
	{
		System.out.println("Der Raum hat .......");
	}
	
	
	//overrides
	@Override
	public String toString()
	{
		return "Dein Held "+held.getName()+" steht gerade in Raum "+aktuellerRaum.getName()+" im Spiel "+getName()+" und h�lt Gegenstand "+
				held.getAktuellerGegenstand()+".";
	}
	
	
	
	
	
	
	
}
