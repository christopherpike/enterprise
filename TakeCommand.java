/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 22.01.2018
 *
 * TakeCommand
 */

import java.util.*;
//package might_and_magic;

class TakeCommand implements IExecute
{
	private String zweitesWort=null;
	
	private Game forbiddenGame=null;
	
	// -----------------
	
	
	//zweitesWort=new String();
	
	//forbiddenGame=new Game();
	
	
	// get / set ---------------
	
	public String getZweitesWort()
	{
		if(null!=zweitesWort)
		{
			return zweitesWort;
		}
		return "?";
	}
	
	//overrides

	/*
	Das Command sollte das item, welches im aktuellen Raum verf�bar ist, aus dem Raum entfernen und in das Inventar des Helden, wenn er
			es noch tragen kann, legen.

	*/

	//Kein toString override: gitb zu Testzwecken das Kommando namentlich zur�ck.
	@Override
	public String bezeichner()
	{
		return "nimm ";
	}

	//overrides
	
	@Override
	public String execute()
	{
		//Nur zum Testen !!!!!!!!!!!!
		//Raum aktuellerRaum=new Raum();

		//Nur zum Testen !!!!!!!!
		Held held=new Held("Vulgo Flammberg");
		
		//addGegenstandZumInventar(Gegenstand gegenstand)
		
		

		return "f�hre aus:";
	}
	
	@Override
	public void setZweitesWort(String zweitesWort)
	{
		
		this.zweitesWort=zweitesWort;				//zweitesWort.split();
	}
	
	@Override
	public int hashCode()
	{
		return "nimm".hashCode();
	}

}
