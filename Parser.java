/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 23.01.2018
 *
 * Parser
 */

import java.util.*;
//package might_and_magic;

class Parser //implements IExecute
{
	private HashMap<String,IExecute>befehle;
	
	//------------------------
	
	public Parser()
	{
		// Hier wird die HashMap initialisiert
		this.befehle=new HashMap<String,IExecute>();
	}
	
	// get / set -------------------
	
	public String getAlleBefehleAlsString()
	{
		Set<String>durchlauf=befehle.keySet();
		
		String rueckgabe="";
		
		for(String str:durchlauf)
		{
			rueckgabe+=str+"\n";
		}
		return rueckgabe;
	}
	
	//Versucht die Befehle aus der Eingabe des Users, Spielers in g�ltige Kommandos umzusetzen
	public IExecute getCommand(String spielerBefehlvomEingabeinterface)
	{	
		if(null!=spielerBefehlvomEingabeinterface)
		{
			
			if(befehle.containsKey(spielerBefehlvomEingabeinterface))
			{
				System.out.println(spielerBefehlvomEingabeinterface);
				System.out.println(befehle.isEmpty());
				return befehle.get(spielerBefehlvomEingabeinterface);
			}
		}	
		return null;
	}
	
	//adder f�r die Befehle-Map
	public void setBefehl(String spielerBefehl,IExecute kommando)
	{
		IExecute lastBefehl=befehle.put(spielerBefehl,kommando);
		//System.out.println(befehle.put(spielerBefehl,kommando));//lastBefehl.bezeichner());
		System.out.println(lastBefehl);
		// isEmpty() boolean  f�llen  containsKey String key  get
		
		System.out.println(befehle.size());
	}
	
}
