/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 23.01.2018
 *
 * Interface Execute
 */

import java.util.*;
//package might_and_magic;

interface IExecute
{
	String execute();
	
	void setZweitesWort(String zweitesWort);

	String bezeichner();
}
