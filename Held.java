/*
 * Emre Tunca, R.Schneider
 * Might and Magic Text-Adventure
 * 19.01.2018
 * //https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository
 * //https://try.github.io/levels/1/challenges/11
 * https://christopherpike@bitbucket.org/christopherpike/enterprise.git
 * https://christopherpike@bitbucket.org/christopherpike/might_and_magic.git
 * https://github.com/git-for-windows/git/releases/download/v2.16.0.windows.2/PortableGit-2.16.0.2-64-bit.7z.exe
 * https://github.com/gitextensions/gitextensions/releases/download/v2.50.02/GitExtensions-2.50.02-Mono.zip
 * Held
 */

import java.io.*;
import java.util.*;

class Held
{
	//Der Spielername
	//Prinz Eisenherz
	private final String name;

	//Das Inventar des Spielers als Lederbeutel
	private LinkedList<Gegenstand>inventar=new LinkedList<Gegenstand>();
	
	//Die Lebensenergie des Heroen, wenn 0 dann ist der Spieler tot.
	private int lebensPunkte=100;
	
	//Die Kraft bestimmt, wie viel dein Held stemmen kann. Die wird durch essbare Gegenst�nde erh�ht, und durch
	// aufgenommenen Gegenst�nde verringert.
	private int kraft=5;
	
	//Liefert eine kurze Beschreibung des aktuellen Gegenstands, den der Spieler gerage in der Hand h�lt
	private Gegenstand aktuellerGegenstand=null;
	
	//--------------------
	
	public Held(String name)
	{
		this.name=name;
	}
	
	// get / set -------------------

	//Liefert den Spielernamen
	public String getName()
	{
		return name;
	}

	//Die Lebensenergie, wenn 0 dann ist der Spieler tot.
	public int getLebensPunkte()
	{
		return lebensPunkte;
	}
	
	//�ndert die Lebensenergie
	public void setLebensPunkte(int lebensPunkte)
	{
		this.lebensPunkte=lebensPunkte;
	}
	
	//Zeigt die Kraft deines Helden an
	public int getKraft()
	{
		return kraft;
	}
	
	//�ndert die Kraft deines Heroen
	public void setKraft(int kraft)
	{
		this.kraft=kraft;
	}

	//Was ist der aktulle Gegenstand
	public Gegenstand getAktuellerGegenstand()
	{
		return aktuellerGegenstand;
	}


	//adder -------------------
	
	//Die Kraft bestimmt, wie viel dein Held stemmen kann. Die wird durch essbare Gegenst�nde erh�ht, und durch
	// aufgenommenen Gegenst�nde verringert.
	//private int kraft=5;
	public boolean addGegenstandZumInventar(Gegenstand gegenstand)
	{
		setKraft( getKraft()-gegenstand.getGewicht() );
		return inventar.add(gegenstand);
	}
	
	public LinkedList<Gegenstand> getInventar()
	{
		return inventar;
	}
	
	//overrides
	
	public String toString()
	{
		return "Dein Held "+getName()+" hat "+getLebensPunkte()+" Lebenspunkte, und tr�gt "+inventar.size()+" Gegenst�nde mit sich herum.";
	}
}
